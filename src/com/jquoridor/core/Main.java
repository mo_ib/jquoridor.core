package com.jquoridor.core;

import com.jquoridor.core.board.*;
import com.jquoridor.core.player.Player;
import com.jquoridor.core.player.PlayerAI;
import com.jquoridor.core.ui.GUI;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args){
        Board board=new Board();
//        board.printVertex();
//        board.printEdge();
//        board.printNeighbours();
        //----------------block------------------------------------------------
//        doBlock((new Position(3,'c')), Direction.VERTICAL, board);
//        doBlock(new Position(4,'d'),Direction.HORIZONTAL, board);
//        doBlock(new Position(4,'f'),Direction.HORIZONTAL, board);
//        doBlock(new Position(4,'h'),Direction.HORIZONTAL, board);
        //-----------------print Board ----------------------------------------
        //GUI.showBoard(board);
        // -----------------Validator Test -------------------------------------
//        AiPlayer player = new AiPlayer(board, 1, new Pawn("RE", new Position(7, 'd')));
//        Action action = new Block(new Position(4, 'e'), "V");
//        Validator<Action> validator = new Validator<>(board, player);
//        System.out.println(validator.checkAction(action));

        //------------------Ai Player test -----------------------------------
//        AiPlayer ai = new AiPlayer(board,1, myPawn);
//        ai.nextAction(board, myPawn);
//        board.showBoard();

        //----------------------Ai GamePlay ----------------------------------
        Player ai1 = new PlayerAI(board, 1, board.getPawnArray().get(0));
        Player ai2 = new PlayerAI(board, 2, board.getPawnArray().get(1));
        ArrayList<Player> players = new ArrayList<>();
        players.add(ai1);
        players.add(ai2);
        Game game = new Game(players, board);
        game.perform();
    }
    public static void doBlock(Position position, Direction direction, Board board){
        int cellId = board.makeCellId(position);
        if(direction == Direction.HORIZONTAL){
            board.getBoard().removeEdge(cellId, cellId +9);
            board.getBoard().removeEdge(cellId+1, cellId+10);
        }
        if(direction == Direction.VERTICAL){
            board.getBoard().removeEdge(cellId, cellId + 1);
            board.getBoard().removeEdge(cellId+9, cellId + 10);
        }
        board.setWallArray(new Wall(position, direction, board.getWallArray().size() + 1));
    }
}

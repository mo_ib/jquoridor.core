package com.jquoridor.core;

import com.jquoridor.core.action.Action;
import com.jquoridor.core.action.ActionValidator;
import com.jquoridor.core.board.Board;
import com.jquoridor.core.board.Direction;
import com.jquoridor.core.board.Position;
import com.jquoridor.core.player.Player;
import com.jquoridor.core.player.PlayerAI;
import com.jquoridor.core.ui.GUI;

import java.util.ArrayList;

public class Game implements Runnable{
    private ArrayList<Player> players;
    private Board board;

    public Game(ArrayList<Player> players, Board board) {
        this.players = players;
        this.board = board;
    }

    @Override
    public void run() {
    }

//    public void perform(){
//        do{
//            for(Player player:players){
//                execute();
//                if(win()){
//                    break;
//                }
//            }
//        }while (true);
//    }
//
//    private boolean win() {
//        return true;
//    }
//
//    private void execute() {
//
//    }

    public void perform(){
        int turn =1;
        GUI.showBoard(board);
        while(turn < 11){
            System.out.println("turn : " + turn);
            for(Player player:players){
                execute(player);
            }
            turn++;
        }
    }

    public ArrayList<Player> getPlayers() {
        return players;
    }

    public void setPlayers(Player player) {
        this.players.add(player);
    }

    public void execute(Player player){
        Action action = player.nextAction(board, player.getPawn());
        ActionValidator validator = new ActionValidator<Action>(board, player.getPawn());
        if(validator.checkAction(action)){
            Position position = action.getPosition();
            Direction direction = action.getDirection();
            if(direction == Direction.NULL){
                player.getPawn().setPawnPosition(position);
                System.out.println(player.getPlayerId() + " : " + "move to " + position.toString());
            }else{
                Main.doBlock(position, direction, board);
                System.out.println(player.getPlayerId() + " : " + "block to " + position.toString() + direction);
            }
            GUI.showBoard(board);
        }else{
            System.out.println("player" + player.getPlayerId() + "your action is wrong");
        }
    }
/*
    //Action have position and direction
    public static void doBlock(Position position, Direction direction, Board board){
        int cellId = board.makeCellId(position);
        if(direction == Direction.HORIZONTAL){
            board.getBoard().removeEdge(cellId, cellId +9);
            board.getBoard().removeEdge(cellId+1, cellId+10);
        }
        if(direction == Direction.VERTICAL){
            board.getBoard().removeEdge(cellId, cellId + 1);
            board.getBoard().removeEdge(cellId+9, cellId + 10);
        }
        board.setWallArray(new Wall(position, direction, board.getWallCounter() + 1));
    }
*/
}

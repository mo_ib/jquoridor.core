package com.jquoridor.core.ui;

import com.jquoridor.core.board.Board;
import com.jquoridor.core.board.Pawn;
import org.jgrapht.graph.DefaultEdge;

import java.util.Scanner;

public class GUI {
    private static Scanner input = new Scanner(System.in);

    public static void showBoard(Board brd){
        System.out.println("-- -- -- -- -- -- -- -- --");
        String line1 = "";
        String line2 = "";
        for(int i=0; i<9; i++){
            line1 = "";
            line2 = "";
            for(int j=0; j<9; j++){
                int cellId = 9*i + j+1;
                String place="";
                for(Pawn pawn:brd.getPawnArray()){
                    if(pawn.getPawnPosition().getID() == cellId){
                        place = pawn.toString();
                        break;
                    }else{
                        //place = brd.getCell(cellId).getCellPosition().toString();
                        place = "  ";
                    }
                }
                line1 += place;
                if(j !=8){
                    if(brd.getBoard().containsEdge(cellId, cellId+1)){
                        line1 += "|";
                    }else{
                        line1 += "#";
                    }
                }else{
                    line1 += "|";
                }
                if(i!=8){
                    if(brd.getBoard().containsEdge(cellId, cellId+9)){
                        line2 += "-- ";
                    }else{
                        line2 += "## ";
                    }
                }else{
                    line2 += "-- ";
                }
            }
            System.out.println(line1);
            System.out.println(line2);
        }
    }

    public static void printVertex(Board brd){
        for(Integer i: brd.getBoard().vertexSet()){
            System.out.println(i + " : " + brd.getCellArray().get(i).getCellPosition().getAddress());
        }
    }
    public static void printEdge(Board brd){
        for(DefaultEdge edge: brd.getBoard().edgeSet()){
            System.out.println(brd.getBoard().getEdgeSource(edge) + " : " + brd.getBoard().getEdgeTarget(edge));
        }
    }
    public static void printNeighbours(Board brd){
        for(Integer i: brd.getBoard().vertexSet()){
            System.out.println(i + " : " + brd.getCellArray().get(i).getNeighbours());
        }
    }

    private static String getInput(){
        String output="";
        output = input.next();
        return output;
    }
    public static String nextHumanAction(){
        System.out.println("Enter your next action:\nM for move\nB for block");
        return getInput();
    }
    public static String nextMoveDirection(){
        System.out.println("Enter your direction:\nU for up\nD for down\nL for left\nR for right");
        return getInput();
    }
}

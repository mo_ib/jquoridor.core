package com.jquoridor.core.player;

import com.jquoridor.core.action.ActionMove;
import com.jquoridor.core.board.Board;
import com.jquoridor.core.board.Direction;
import com.jquoridor.core.board.Pawn;
import com.jquoridor.core.action.Action;
import com.jquoridor.core.board.Position;
import com.jquoridor.core.ui.GUI;

public class PlayerHuman implements Player {
    private Integer playerId;
    private Pawn pawn;

    public PlayerHuman(Integer playerId, Pawn pawn) {
        this.playerId = playerId;
        this.pawn = pawn;
    }

    @Override
    public void run() {

    }

    @Override
    public Action nextAction(Board board, Player player) {
        Action output;
//        String act = GUI.nextHumanAction();
//        Position nextPosition = new Position(0,'a');
//        Direction nextDirection;
//        if (act.equals("M")){
//            act = GUI.nextMoveDirection();
//            if (act.equals("D")){
//                nextPosition = new Position(player.getPawn().getPawnPosition().getRow()+1, player.getPawn().getPawnPosition().getCol());
//            }
//            if (act.equals("U")){
//                nextPosition = new Position(player.getPawn().getPawnPosition().getRow()-1, player.getPawn().getPawnPosition().getCol());
//            }
//            if (act.equals("L")){
//                nextPosition = new Position(player.getPawn().getPawnPosition().getRow(), ()player.getPawn().getPawnPosition().getCol());
//            }
//            if (act.equals("R")){
//                nextPosition = new Position(player.getPawn().getPawnPosition().getRow(), player.getPawn().getPawnPosition().getCol());
//            }
//            output = new ActionMove(nextPosition);
//        }
        return null;
    }

    @Override
    public Pawn getPawn() {
        return pawn;
    }

    @Override
    public Integer getPlayerId() {
        return playerId;
    }
}

package com.jquoridor.core.player;

import com.jquoridor.core.board.Board;
import com.jquoridor.core.board.Pawn;
import com.jquoridor.core.action.Action;

public interface Player extends Runnable {
    public Action nextAction(Board board, Player player);
    public Pawn getPawn();
    public Integer getPlayerId();
}

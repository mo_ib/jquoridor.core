package com.jquoridor.core.player;

import com.jquoridor.core.action.ActionBlock;
import com.jquoridor.core.board.Board;
import com.jquoridor.core.board.Direction;
import com.jquoridor.core.board.Pawn;
import com.jquoridor.core.board.Position;
import com.jquoridor.core.action.Action;
import com.jquoridor.core.action.ActionMove;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;

public class PlayerStrategy {
    private Board board;
    private Pawn myPawn;
    public PlayerStrategy(Board board, Pawn pawn) {
        this.board=board;
        this.myPawn=pawn;
    }

    private Position findOpponentPosition(){
        Position out = myPawn.getPawnPosition();
        for(Pawn pawn:board.getPawnArray()){
            if(!pawn.equals(myPawn)){
                out = pawn.getPawnPosition();
            }
        }
        return out;
    }

    public GraphPath<Integer, DefaultEdge> shortPath(Integer source, Integer target){
        DijkstraShortestPath<Integer, DefaultEdge> path = new DijkstraShortestPath<>(board.getBoard());
        return path.getPath(source, target);
    }

    public Action strategy1(){
        int myNumberId = myPawn.getPawnPosition().getID();
        int myTarget =82;
        int oppTarget = 0;
        if(myPawn.getColor().equals("RE")){
            myTarget = 0;
            oppTarget = 82;
        }
        int opponentNumberId = findOpponentPosition().getID();

        GraphPath<Integer, DefaultEdge> myPath = shortPath(myNumberId, myTarget);
        GraphPath<Integer, DefaultEdge> oppPath = shortPath(opponentNumberId, oppTarget);

        if(myPath.getLength() <= oppPath.getLength()){
            return new ActionMove(nextMove(myPath.getEdgeList().get(0), myNumberId));
        }else{
            return nextBlock(oppTarget);
        }
    }

    private Position nextMove(DefaultEdge edge, int numberId){
        if(board.getBoard().getEdgeSource(edge) == numberId){
            return board.getCell(board.getBoard().getEdgeTarget(edge)).getCellPosition();
        }
        return board.getCell(board.getBoard().getEdgeSource(edge)).getCellPosition();
    }

    private Action nextBlock(int oppTarget){
        Position outPosition = myPawn.getPawnPosition();
        Direction outDirection = Direction.NULL;

        Position opponentCurrentPosition = findOpponentPosition();
        int opponentCurrentPositionId = opponentCurrentPosition.getID();

        GraphPath<Integer, DefaultEdge> oppPath = shortPath(opponentCurrentPositionId, oppTarget);
        DefaultEdge edge = oppPath.getEdgeList().get(0);

        Position nextOpponentPosition = findVertex(edge, opponentCurrentPositionId);
        Integer nextOpponentPositionId = nextOpponentPosition.getID();

        if(nextOpponentPositionId - opponentCurrentPositionId == -1){
            outPosition = nextOpponentPosition;
            outDirection = Direction.VERTICAL;
        }else if(nextOpponentPositionId - opponentCurrentPositionId == 1){
            outPosition = opponentCurrentPosition;
            outDirection = Direction.VERTICAL;
        }else if(nextOpponentPositionId - opponentCurrentPositionId == 9){
            outPosition = nextOpponentPosition;
            outDirection = Direction.HORIZONTAL;
        }else if(nextOpponentPositionId - opponentCurrentPositionId == -9){
            outPosition = nextOpponentPosition;
            outDirection = Direction.HORIZONTAL;
        }
        return new ActionBlock(outPosition, outDirection);
    }

    private Position findVertex(DefaultEdge edge, int numberId){
        if(board.getBoard().getEdgeSource(edge) == numberId){
            return board.getCell(board.getBoard().getEdgeTarget(edge)).getCellPosition();
        }
        return board.getCell(board.getBoard().getEdgeSource(edge)).getCellPosition();
    }
}

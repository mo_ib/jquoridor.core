package com.jquoridor.core.player;

import com.jquoridor.core.board.Board;
import com.jquoridor.core.board.Pawn;
import com.jquoridor.core.action.Action;

public class PlayerAI implements Player {
    private Integer playerId;
    private Pawn pawn;
    private Board board;

    public PlayerAI(Board board, Integer playerId, Pawn pawn) {
        this.playerId = playerId;
        this.pawn = pawn;
        this.board = board;
    }

    public Integer getPlayerId() {
        return playerId;
    }

    public Pawn getPawn() {
        return pawn;
    }

    @Override
    public void run() {
    }

    public Action nextAction(Board board, Player player){
        PlayerStrategy playerStrategy = new PlayerStrategy( board, player.getPawn() );
        return playerStrategy.strategy1();
//        System.out.println(action.getPosition() + " , " + action.getStatus());
//        if(action.getStatus() == null){
//            pawn.setPosition(action.getPosition());
//        }else{
//            board.block(action.getPosition(), action.getStatus());
//        }
    }
}
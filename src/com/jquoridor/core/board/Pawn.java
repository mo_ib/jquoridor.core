package com.jquoridor.core.board;

public class Pawn extends BoradItem{
    private String color;
    private Position pawnPosition;

    public Pawn(String color){
        if(color.equals("RE")){
            this.color = color;
            this.pawnPosition = new Position(9,'e');
        }
        if(color.equals("BL")){
            this.color = color;
            this.pawnPosition = new Position(1,'e');
        }
    }

    public Pawn(String color, Position pawnPosition) {
        this.color = color;
        this.pawnPosition = pawnPosition;
    }

    public String getColor() {
        return color;
    }

    public Position getPawnPosition() {
        return pawnPosition;
    }

    @Override
    public String toString() {
        return color;
    }

    public void setPawnPosition(Position pawnPosition) {
        this.pawnPosition = pawnPosition;
    }
}


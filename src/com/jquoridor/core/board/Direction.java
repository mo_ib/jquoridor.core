package com.jquoridor.core.board;

public enum Direction {
    HORIZONTAL, VERTICAL, NULL
}

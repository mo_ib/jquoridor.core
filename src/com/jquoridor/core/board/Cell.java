package com.jquoridor.core.board;

import java.util.ArrayList;

public class Cell extends BoradItem{
    private int cellId;
    private boolean isEmpty;
    private Position cellPosition;
    private ArrayList<Integer> neighbours;

    public Cell(int cellId, Position cellPosition) {
        this.cellId = cellId;
        this.cellPosition = cellPosition;
        this.isEmpty=true;
        neighbours= new ArrayList<>();
    }

    public int getCellId() {
        return cellId;
    }

    public void setCellId(int cellId) {
        this.cellId = cellId;
    }

    public boolean isEmpty() {
        return isEmpty;
    }

    public void setEmpty(boolean empty) {
        isEmpty = empty;
    }

    public Position getCellPosition() {
        return cellPosition;
    }

    public Integer getNeighbours(int index){
        return this.neighbours.get(index);
    }

    public ArrayList<Integer> getNeighbours() {
        return neighbours;
    }

    public void setNeighbours(Integer cellId) {
        if( !(cellId<1) && !(cellId>82) ){
            this.neighbours.add(cellId);
        }
    }

    public void removeNeighbour(Integer cellId){
        this.neighbours.remove(cellId);
    }
}

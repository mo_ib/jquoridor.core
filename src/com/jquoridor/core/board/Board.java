package com.jquoridor.core.board;

import org.jgrapht.graph.SimpleGraph;
import org.jgrapht.graph.DefaultEdge;
import java.util.ArrayList;

public class Board {
    private SimpleGraph<Integer, DefaultEdge> board;
    private ArrayList<Cell> cellArray;
    private ArrayList<Wall> wallArray;
    private ArrayList<Pawn> pawnArray;

    public Board() {
        this.board = new SimpleGraph<Integer, DefaultEdge>(DefaultEdge.class);
        this.cellArray = new ArrayList<Cell>(83);
        this.wallArray = new ArrayList<Wall>();
        this.pawnArray = new ArrayList<Pawn>();
        initializeCellArray();
        initializePawnArray();
    }

    //Getter Methods
    public Cell getCell(Position position){
        return cellArray.get(makeCellId(position));
    }
    public Cell getCell(Integer cellId){
        return cellArray.get(cellId);
    }
    public ArrayList<Pawn> getPawnArray() {
        return pawnArray;
    }
    public ArrayList<Wall> getWallArray() {
        return wallArray;
    }
    public ArrayList<Cell> getCellArray() {
        return cellArray;
    }
    public SimpleGraph<Integer, DefaultEdge> getBoard() {
        return board;
    }

    //Setter Methods
    public void setCellArray(Cell cell) {
        this.cellArray.add(cell);
    }
    public void setWallArray(Wall wall) {
        this.wallArray.add(wall);
    }
    public void setPawnArray(Pawn pawn) {
        this.pawnArray.add(pawn);
    }

    private void initializeCellArray() {
        //up invisible cell
        Cell up = new Cell(0, new Position(0,'j'));
        cellArray.add(up);
        board.addVertex(0);

        //create cells, setting it's neighbours, adding vertex to board
        for(int i=1; i<=9; i++){
            for(char ch='a'; ch<='i'; ch++){
                Integer cellId = 9*(i-1)+((int)ch -97)+1;
                Cell cell = new Cell(cellId, new Position(i,ch));
                if (ch!='a'){
                    cell.setNeighbours(cellId-1);
                }
                if (ch!='i'){
                    cell.setNeighbours(cellId+1);
                }
                cell.setNeighbours(cellId-9);
                cell.setNeighbours(cellId+9);
                cellArray.add(cell);
                board.addVertex(cellId);
            }
        }
        //down invisible cell
        Cell down = new Cell(82, new Position(0, 'k'));
        cellArray.add(down);
        board.addVertex(82);

        for(int i=1; i<=9; i++){
            board.addEdge(0, i);
        }
        for(int i=73; i<=81; i++){
            board.addEdge(82, i);
        }

        for(Cell cell: cellArray){
            ArrayList<Integer> array = cell.getNeighbours();
            for(Integer integer:array){
                board.addEdge(cell.getCellId(), integer);
            }
        }
    }
    private void initializePawnArray() {
        Pawn pawn1 = new Pawn("RE");
        setPawnArray(pawn1);
        getCell(new Position(9,'e')).setEmpty(false);

        Pawn pawn2 = new Pawn("BL");
        setPawnArray(pawn2);
        getCell(new Position(1,'e')).setEmpty(false);
    }

    public Integer makeCellId(Position position){
        return 9*(position.getRow() -1) + ((int)position.getCol()-97) + 1;
    }

}

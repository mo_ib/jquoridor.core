package com.jquoridor.core.board;

import java.util.Objects;

public class Position {
    private Integer id;
    private Integer row;
    private Character col;

    public Position(Integer row, Character col) {
        this.row = row;
        this.col = col;
        this.id = getID();
    }

    public Integer getRow() {
        return row;
    }

    public Character getCol() {
        return col;
    }

    public String getAddress(){
        return "" + this.row + this.col;
    }

    public Integer getID(){
        this.id = 9*(row - 1) + ((int)col - 97) + 1;
        return id;
    }

    @Override
    public String toString() {
        return "" + this.row + this.col;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Position)) return false;
        Position position = (Position) o;
        return Objects.equals(row, position.row) &&
                Objects.equals(col, position.col);
    }

    @Override
    public int hashCode() {
        return Objects.hash(row, col);
    }
}

package com.jquoridor.core.board;

public class Wall extends BoradItem{
    private Integer wallId;
    private Direction direction;
    private Position wallPosition;

    public Wall(Position wallPosition, Direction direction, Integer wallId ) {
        this.wallId = wallId;
        this.direction = direction;
        this.wallPosition = wallPosition;
    }

    public Integer getWallId() {
        return wallId;
    }

    public Direction getDirection() {
        return direction;
    }

    public Position getWallPosition() {
        return wallPosition;
    }
}
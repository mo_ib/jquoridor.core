package com.jquoridor.core.action;

import com.jquoridor.core.board.Direction;
import com.jquoridor.core.board.Position;

public class ActionBlock implements Action {
    private Position position;
    private Direction direction;

    public ActionBlock(Position position, Direction direction){
        this.position = position;
        this.direction = direction;
    }

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public Direction getDirection() {
        return direction;
    }
}

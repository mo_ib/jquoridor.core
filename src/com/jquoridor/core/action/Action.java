package com.jquoridor.core.action;

import com.jquoridor.core.board.Direction;
import com.jquoridor.core.board.Position;

public interface Action {
    public abstract Position getPosition();
    public abstract Direction getDirection();
}

package com.jquoridor.core.action;

import com.jquoridor.core.board.Direction;
import com.jquoridor.core.board.Position;

public class ActionMove implements Action{
    private Position nextPosition;

    public ActionMove(Position nextPosition){
        this.nextPosition = nextPosition;
    }

    public Position getPosition() {
        return nextPosition;
    }

    public Direction getDirection(){
        return Direction.NULL;
    }
}

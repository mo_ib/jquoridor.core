package com.jquoridor.core.action;

import com.jquoridor.core.board.*;
import com.jquoridor.core.player.Player;
import com.jquoridor.core.player.PlayerAI;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.shortestpath.DijkstraShortestPath;
import org.jgrapht.graph.DefaultEdge;

public class ActionValidator<T extends Action> {
    Board board;
    Pawn pawn;

    public ActionValidator(Board board, Pawn pawn){
        this.board = board;
        this.pawn = pawn;
    }

    public Boolean makeChoice(T action){
        return true;
    }

    Player player;

    public ActionValidator(Board board, Player player){
        this.board = board;
        this.player = player;
    }

    public Boolean checkAction(T action){
        Boolean out = false;
        Position nextPosition = action.getPosition();
        Direction direction = action.getDirection();
        if(direction == null){
            if(board.getCell(nextPosition).isEmpty()){
                if(board.getCell(player.getPawn().getPawnPosition()).getNeighbours().contains(board.makeCellId(nextPosition))){
                    out = true;
                }
                if(!board.getCell(player.getPawn().getPawnPosition()).getNeighbours().contains(board.makeCellId(nextPosition))){
                    if (canJump(nextPosition)){
                        out = true;
                    }
                    if(canMoveL(nextPosition)){
                        out = true;
                    }
                }
            }else {
                System.out.println("you can't go there. it's not empty");
            }
        }else{
            if(canBlock(nextPosition, direction)){
                out = true;
            }
        }
        return out;
    }

    private Boolean canJump(Position nextPosition){
        Boolean out = false;
        if(!board.getCell(player.getPawn().getPawnPosition()).getNeighbours().contains(board.makeCellId(nextPosition))){
            int now = board.makeCellId(player.getPawn().getPawnPosition());
            int next = board.makeCellId(nextPosition);
            int differ = next - now;
            if(differ == -18){
                if(board.getBoard().containsEdge(now, now -9 ) &&  !(board.getCell(now-9).isEmpty())){
                    if(board.getBoard().containsEdge(now -9, next)){
                        out = true;
                    }
                }
            }

            if(differ == 18){
                if(board.getBoard().containsEdge(now, now +9  ) &&  !(board.getCell(now+9).isEmpty())){
                    if(board.getBoard().containsEdge(now +9, next)){
                        out = true;
                    }
                }
            }
            if(differ == 2){
                if(board.getBoard().containsEdge(now, now + 1) &&  !(board.getCell(now+1).isEmpty())){
                    if(board.getBoard().containsEdge(now +1, next)){
                        out = true;
                    }
                }
            }

            if(differ == -2){
                if(board.getBoard().containsEdge(now, now -1 ) &&  !(board.getCell(now-1).isEmpty())){
                    if(board.getBoard().containsEdge(now -1, next)){
                        out = true;
                    }
                }
            }
        }
        return out;
    }

    private Boolean canMoveL(Position nextPosition){
        Boolean out = false;
        Cell myCell = board.getCell(player.getPawn().getPawnPosition());
        int nextId = board.getCell(nextPosition).getCellId();
        for(Integer i:myCell.getNeighbours()){
            if(i != myCell.getCellId() && !board.getCell(i).isEmpty() && board.getBoard().containsEdge(i, nextId)){
                out = true;
            }
        }
        return out;
    }

    private Boolean canBlock(Position position, Direction status){
        Boolean out = true;
        for (Wall wall:board.getWallArray()){
            if(wall.getWallPosition().equals(position)){
                out = false;
            }else if(status.equals("V")){
                Character ch = position.getCol();
                if(position.getRow() == 9 || position.getCol() == 'i'){
                    out = false;
                }else if(wall.getWallPosition().equals(new Position(position.getRow()-1,ch))){
                    out = false;
                }else if(wall.getWallPosition().equals(new Position(position.getRow() +1,ch))){
                    out = false;
                }

            }else if(status.equals("H")){
                Character ch = position.getCol();
                if(position.getCol() == 'i' || position.getRow() == 9){
                    out = false;
                }else if(wall.getWallPosition().equals(new Position(position.getRow(),--ch))){
                    out = false;
                }else if(wall.getWallPosition().equals(new Position(position.getRow(),++ch))){
                    out = false;
                }
            }
            if(!isAPath()){
                out = false;
            }
        }
        return out;
    }

    public Boolean isAPath(){
        Boolean out = true;
        Pawn myPawn = this.pawn;
        Pawn oppPawn = findOpponentPawn(myPawn);
        int myTarget =82;
        int oppTarget = 0;
        if(myPawn.getColor().equals("RE")){
            myTarget = 0;
            oppTarget = 82;
        }
        GraphPath<Integer, DefaultEdge> myPath = shortPath(myPawn.getPawnPosition().getID(), myTarget);
        GraphPath<Integer, DefaultEdge> oppPath = shortPath(oppPawn.getPawnPosition().getID(), oppTarget);
        if(myPath == null || oppPath == null){
            out = false;
        }
        return out;
    }

    public Pawn findOpponentPawn(Pawn myPawn){
        Pawn outPawn = myPawn;
        for(Pawn pawn:board.getPawnArray()){
            if(!pawn.equals(myPawn)){
                outPawn = pawn;
            }
        }
        return outPawn;
    }

    private GraphPath<Integer, DefaultEdge> shortPath(Integer source, Integer target){
        DijkstraShortestPath<Integer, DefaultEdge> path = new DijkstraShortestPath<>(board.getBoard());
        return path.getPath(source, target);
    }

}
